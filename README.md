* Le [pdf Agile Mans 2018](https://gitlab.com/tclavier/atelier-legotdd/builds/artifacts/20180118-AgileMans/file/2018agilemans.pdf?job=compile_pdf)
* Le [pdf Agile Laval 2017](https://gitlab.com/tclavier/atelier-legotdd/builds/artifacts/20170629-AgileLaval/file/2017agileLaval.pdf?job=compile_pdf)
* Le [pdf Agile Rennes 2017](https://gitlab.com/tclavier/atelier-legotdd/builds/artifacts/20171013-atrennes/file/2017AgileRennes.pdf?job=compile_pdf)
* Les [supports](https://gitlab.com/tclavier/atelier-legotdd/builds/artifacts/20171113-atlille/file/slides.pdf?job=compile_pdf) et le [pitch](https://gitlab.com/tclavier/atelier-legotdd/builds/artifacts/20171113-atlille/file/pitch.pdf?job=compile_pdf) Agile tour Lille 2017 
* Le [pdf Agile Day BNP Paribas 2017](https://gitlab.com/tclavier/atelier-legotdd/builds/artifacts/20171120-AgileDayBNP/file/slides.pdf?job=compile_pdf)
